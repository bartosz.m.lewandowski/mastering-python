# Mastering Python



## About
This repository is used to collect useful learnings about the python language, such as: 

- memory usage,
- data structures,
- typing,
- object oriented programing,
- test-driven development,

and so on.


## Sources
- https://pythonspeed.com/datascience/
- https://www.youtube.com/@ArjanCodes