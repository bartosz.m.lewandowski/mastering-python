import numpy as np
from pympler import asizeof
import sys

print(
    "Size of small integer objects in Python is {}-bytes.".format(asizeof.asizeof(123))
)

list_of_numbers = []
for i in range(1000000):
    list_of_numbers.append(i)

print(
    "Size of 1mln integers list is {}-bytes where ~8MB is for pointers (1 pointer is 8-byte) and ~32MB fo objects (1 object is 32-bytes).".format(
        asizeof.asizeof(list_of_numbers)
    )
)

arr = np.zeros((1000000,), dtype=np.uint64)
for i in range(1000000):
    arr[i] = i

print(
    "Size of 1mln integers array is {}-bytes because NumPy arrays that are storing numbers don't store references to Python objects.".format(
        asizeof.asizeof(arr)
    )
)
