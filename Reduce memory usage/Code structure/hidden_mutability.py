import numpy as np
from memory_profiler import profile


# 3*A bytes of RAM - wynika z zapisania w pamieci: oryginalnej listy, czasowej listy array - low oraz returna
@profile
def normalize(array: np.ndarray) -> np.ndarray:
    """_summary_

    Args:
        Takes a floating point array.

    Returns:
        Returns a normalized array with values between 0 and 1.
    """
    low = array.min()
    high = array.max()

    return (array - low) / (high - low)


# A bytes of RAM - poniewaz tylko modyfikujemy oryginalna liste
@profile
def normalize_in_place(array: np.ndarray) -> np.ndarray:
    low = array.min()
    high = array.max()
    array -= low
    array /= high - low


# 2*A bytes of RAM - rozwiazanie posrodku
@profile
def normilize_hybrid(array: np.ndarray) -> np.ndarray:
    low = array.min()
    high = array.max()
    result = array.copy()
    result -= low
    result /= high - low
    return result


array = np.random.uniform(low=30.0, high=60.0, size=100)

if __name__ == "__main__":
    normalize(array)
    print("Primary data not changed:\n", array)
    normilize_hybrid(array)
    print("Primary data not changed:\n", array)
    normalize_in_place(array)
    print("Problem of modification of primary data:\n", array)
