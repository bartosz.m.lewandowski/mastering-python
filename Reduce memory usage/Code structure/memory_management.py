import numpy as np
from memory_profiler import profile


# 1GB
@profile
def load_1GB_of_data():
    return np.ones((2**30), dtype=np.uint8)


# Peak = 3GB - due to "holding" first modified array (modify1()) until using modified array with modify2()
# while we have local variable data = load_1GB_of_data() <- this is unnecessary/redundant
@profile
def process_data():
    data = load_1GB_of_data()  # <- `data` var lives too long
    return modify2(modify1(data))


# 1GB
@profile
def modify1(data):
    return data * 2


# 1GB
@profile
def modify2(data):
    return data + 10


# Solution 1 - remove local variable, peak = 2GB
@profile
def process_data_1():
    return modify2(modify1(load_1GB_of_data()))


# Solution 2 - reuse local variable, peak = 2GB
@profile
def process_data_2():
    data = load_1GB_of_data()
    data = modify1(data)
    data = modify2(data)
    return data


# Solution 3 - transfer object ownership, peak = 2GB
class Owner:
    def __init__(self, data):
        self.data = data


@profile
def process_data_3():
    data = Owner(load_1GB_of_data())
    return modify2(modify_1(data))


@profile
def modify_1(owned_data):
    data = owned_data.data
    # Remove a reference to original data:
    owned_data.data = None
    return data * 2


if __name__ == "__main__":
    process_data()
    process_data_1()
    process_data_2()
    process_data_3()
