from random import random
from pympler import asizeof
import pandas as pd
import sys


class Point:
    def __init__(self, x):
        self.x = x


objects = []
for _ in range(1000000):
    r = random()
    point = Point(r)
    objects.append(point)

print("The size of list of objects is {}-bytes.".format(asizeof.asizeof(objects)))
print("Head of list of objects:\n{}".format(objects[0:2]))


# Solution 1 - remove dictionary, store slots # https://stackoverflow.com/questions/472000/usage-of-slots
class Point_1:
    __slots__ = ["x"]  # <-- allowed attributes

    def __init__(self, x):
        self.x = x


objects = []
for _ in range(1000000):
    r = random()
    point = Point_1(r)
    objects.append(point)

print(
    "The size of list of objects (storing __slots__ instead of __dict__) is {}-bytes.".format(
        asizeof.asizeof(objects)
    )
)
print("Head of list of objects:\n{}".format(objects[0:2]))

# Solution 2 - remove objects, create own dictionary
points = {
    "x": [],
}

for _ in range(1000000):
    r = random()
    points["x"].append(r)

print("The size of custom dictionary is {}-bytes.".format(asizeof.asizeof(points)))
print("Head of list of objects from dictionary:\n{}".format(points["x"][0:10]))

# Solution 3 - PANDAS!
objects = []
for _ in range(1000000):
    r = random()
    objects.append(r)

df = pd.DataFrame(objects, columns=["x"])

print("The size of pandas data frame is {}-bytes.".format(asizeof.asizeof(df)))
print("Head of pandas data frame:\n{}".format(df.head()))
