import pandas as pd
import tracemalloc
import time
import sqlite3


# Basic chunking
def get_voters_in_state(name):
    return pd.concat(
        df[df["state"] == name]
        for df in pd.read_csv("./Reduce memory usage/Pandas/Voters.csv", chunksize=50)
    )


start = time.time()
tracemalloc.start()
alaska = get_voters_in_state("ALASKA")
alaska.head()
print(
    "Size of DataFrame is {}-bytes with {} rows.".format(
        alaska.memory_usage(deep=True).sum(), alaska.shape[0]
    )
)
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()
end = time.time()
print("Execution time was {} seconds.".format(end - start))

# Using SQLite
# create a new database file
db = sqlite3.connect("./Reduce memory usage/Pandas/Voters.sqlite")

# loda the csv in chunks
for c in pd.read_csv("./Reduce memory usage/Pandas/Voters.csv", chunksize=50):
    # append all rows to a new database table, which we name 'voters'
    c.to_sql("voters", db, if_exists="append")
# add an index on the "state" column
db.execute("CREATE INDEX state on Voters(state)")
db.close()

db.execute("SELECT * FROM Voters WHERE state = 'ALASKA'")


# Chunking with SQLite
def get_voters_in_state(name):
    conn = sqlite3.connect("./Reduce memory usage/Pandas/Voters.sqlite")
    q = "SELECT * FROM Voters WHERE state = '{}'".format(name)
    return pd.read_sql_query(q, conn)


start = time.time()
tracemalloc.start()
alaska = get_voters_in_state("ALASKA")
alaska.head()
print(
    "Size of DataFrame is {}-bytes with {} rows.".format(
        alaska.memory_usage(deep=True).sum(), alaska.shape[0]
    )
)
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()
end = time.time()
print("Execution time was {} seconds.".format(end - start))
# ~85% faster
