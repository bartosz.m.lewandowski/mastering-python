import pandas as pd
import sys
import tracemalloc


# Fixed-sized object - 64-bit integer
series = pd.Series([123] * 1_000_000)
print(
    "According to Pandas API, memory usage of this series is {}-bytes.".format(
        series.memory_usage()
    )
)
print(
    "According to Pandas API, memory usage of this series (with deep=True) is {}-bytes.".format(
        series.memory_usage(deep=True)
    )
)
# For any type that has a fixed size in memory–integers, floats, categoricals, and so on–both memory_usage()
# variants should give the same answer, and a pretty accurate one.

# Arbitrarily-sized objects (obiekty dowolnego rozmiaru)
series = pd.Series(["abcdefhjiklmnopqrstuvwxyz" * 10 for i in range(1_000_000)])
print(
    "According to Pandas API, memory usage of this series is {}-bytes.".format(
        series.memory_usage()
    )
)
print(
    "According to Pandas API, memory usage of this series (with deep=True) is {}-bytes.".format(
        series.memory_usage(deep=True)
    )
)

print(
    "Because by default, pandas do not count memory being used by the strings themselfes, then the actual memory beeing used is {}-bytes.".format(
        sum([sys.getsizeof(s) for s in series]) + series.memory_usage()
    )
)
# Seems like pandas repaired this :)

# Smart python
tracemalloc.start()
series = pd.Series(["abcdefhjiklmnopqrstuvwxyz" * 10 for i in range(1_000_000)])
print(
    "Peak memory usage was {}-bytes, while size of the series is {}-bytes.".format(
        tracemalloc.get_traced_memory()[1], series.memory_usage(deep=True)
    )
)
tracemalloc.stop()
# it is because of memory optimization for string which is deafult in Python

tracemalloc.start()
s = "abcdefghijklmnopqrstuvwxyz" * 10
s2 = "abcdefghijklmnopqrstuvwxyz" * 10
s is s2
print(
    "Peak memory usage was {}-bytes, while size of the series is {}-bytes.".format(
        tracemalloc.get_traced_memory()[1], sys.getsizeof(s)
    )
)
tracemalloc.stop()
# python was smart enough to store it only once, saving lots of memory
