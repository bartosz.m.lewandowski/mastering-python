import pandas as pd
from sqlalchemy import create_engine, text
import tracemalloc


"""
# DEFINE THE DATABASE CREDENTIALS - template
user = 'root'
password = 'password'
host = '127.0.0.1'
port = 3306
database = 'GeeksForGeeks'

url="mysql+pymysql://{0}:{1}@{2}:{3}/{4}".format(user, password, host, port, database)

password_ = input()
engine = create_engine("postgresql://postgres:{}@localhost:5432/mastering_python".format(password_))
engine.connect()
"""


# Technique #1 - simple loading
def process_sql_using_pandas(query: str, password: str):
    engine = create_engine(
        "postgresql://postgres:{}@localhost:5432/mastering_python".format(password)
    )
    conn = engine.connect()
    dataframe = pd.read_sql_query(text(query), conn)
    print(
        f"Got DataFrame of size {dataframe.memory_usage(deep=True).sum()}-bytes with {len(dataframe)} entries"
    )


if __name__ == "__main__":
    tracemalloc.start()
    print("Enter password for postgreSQL:")
    password_ = input()
    process_sql_using_pandas(query="SELECT * FROM voters", password=password_)
    print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
    tracemalloc.stop()


# Technique #2 - (imperfect) batching
def process_sql_using_pandas(query: str, password: str):
    engine = create_engine(
        "postgresql://postgres:{}@localhost:5432/mastering_python".format(password)
    )
    conn = engine.connect()

    for chunk in pd.read_sql_query(text(query), conn, chunksize=500):
        print(
            f"Got DataFrame chunk of size {chunk.memory_usage(deep=True).sum()} with {len(chunk)} rows."
        )


if __name__ == "__main__":
    tracemalloc.start()
    print("Enter password for postgreSQL:")
    password_ = input()
    process_sql_using_pandas(query="SELECT * FROM voters", password=password_)
    print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
    tracemalloc.stop()

# SQLAlchemy loads all the data into memory, and then hands the Pandas API 1000 rows at a time,
# but from local memory. If our data is large enough, it still won’t fit in memory.


# Technique #3 - (proper) batching, i.e. use server-side cursors, aka streaming
def process_sql_using_pandas(query: str, password: str):
    engine = create_engine(
        "postgresql://postgres:{}@localhost:5432/mastering_python".format(password)
    )
    conn = engine.connect().execution_options(stream_results=True)

    for chunk in pd.read_sql_query(text(query), conn, chunksize=500):
        print(
            f"Got DataFrame chunk of size {chunk.memory_usage(deep=True).sum()} with {len(chunk)} rows."
        )


if __name__ == "__main__":
    tracemalloc.start()
    print("Enter password for postgreSQL:")
    password_ = input()
    process_sql_using_pandas(query="SELECT * FROM voters", password=password_)
    print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
    tracemalloc.stop()

# Instead of loading all rows into memory, it will only load rows from the database
# when they’re requested by the user, in this case Pandas. This works with multiple engines,
# like Oracle and MySQL, it’s not just limited to PostgreSQL.
