import pandas as pd
import polars as pl
import pyarrow.parquet as pq
import fastparquet as fast_pq
import tracemalloc
import time


df = pd.read_csv(
    "./Reduce memory usage/Pandas/MBTA_Bus_Arrival_Departure_Times_2022/MBTA-Bus-Arrival-Departure-Times_2022-05.csv"
)

print(
    "Size of DataFrame with default dtypes {}-bytes.".format(
        df.memory_usage(deep=True).sum()
    )
)
# Size of DataFrame with default dtypes 1376281720-bytes.

df = pd.read_csv(
    "./Reduce memory usage/Pandas/MBTA_Bus_Arrival_Departure_Times_2022/MBTA-Bus-Arrival-Departure-Times_2022-05.csv",
    dtype={
        "route_id": "string",
    },
    parse_dates=["service_date", "scheduled", "actual"],
)
for categorical_col in ["route_id", "direction_id", "point_type", "standard_type"]:
    df[categorical_col] = df[categorical_col].astype("category")

print(
    "Size of DataFrame with (some) 'custom' dtypes {}-bytes.".format(
        df.memory_usage(deep=True).sum()
    )
)

# Saving frame into (working) parquet
df.to_parquet(
    "./Reduce memory usage/Pandas/MBTA-Bus-Arrival-Departure-Times_2022-05.parquet"
)
# Size of DataFrame with (some) 'custom' dtypes 311073539-bytes.


# Algorithm
# 1. Get rid of all rows that aren’t headway information.
# 2. Calculate the ratio of actual headway to expected headway; if it’s bigger than 1, that means the bus arrived late.
# 3. For every pair of route number and direction (inbound/outbound), pick the median headway ratio for the month.
# 4. Find the 5 route pairs with the worst median ratio.


# Naive implementation
def find_worst_headways():
    data = pd.read_parquet(
        "./Reduce memory usage/Pandas/MBTA-Bus-Arrival-Departure-Times_2022-05.parquet"
    )

    data = data[data["standard_type"] == "Headway"]  # 1

    data["headway_ratio"] = data["headway"] / data["scheduled_headway"]  # 2

    by_route = data.groupby(["route_id", "direction_id"])  # 3
    median_headway = by_route[["headway_ratio"]].median()

    return median_headway.nlargest(5, columns=["headway_ratio"])  # 4


tracemalloc.start()
start = time.time()
print(find_worst_headways())
end = time.time()
print(
    "Peak memory usage was {}-bytes and computation took {} seconds.".format(
        tracemalloc.get_traced_memory()[1], end - start
    )
)
tracemalloc.stop()
# Peak memory usage was 182559870-bytes and computation took 0.4730031490325928 seconds.


# Load in chunks (with pyarrow.parquet)
def find_worst_headways():
    chunks = []
    parquet_file = pq.ParquetFile(
        "./Reduce memory usage/Pandas/MBTA-Bus-Arrival-Departure-Times_2022-05.parquet"
    )
    # 1
    for batch in parquet_file.iter_batches():
        chunk = batch.to_pandas()
        del batch
        # 2
        chunk["headway_ratio"] = chunk["headway"] / chunk["scheduled_headway"]

        chunks.append(chunk[["route_id", "direction_id", "headway_ratio"]])
    del parquet_file

    data = pd.concat(chunks)
    del chunks

    # 3
    by_route = data.groupby(["route_id", "direction_id"])
    median_headway = by_route[["headway_ratio"]].median()

    # 4
    return median_headway.nlargest(5, columns=["headway_ratio"])


tracemalloc.start()
start = time.time()
print(find_worst_headways())
end = time.time()
print(
    "Peak memory usage was {}-bytes and computation took {} seconds.".format(
        tracemalloc.get_traced_memory()[1], end - start
    )
)
tracemalloc.stop()
# Peak memory usage was 158445183-bytes and computation took 0.8801295757293701 seconds.


# Naive implementation (with fastparquet engine)
def find_worst_headways():
    data = pd.read_parquet(
        "./Reduce memory usage/Pandas/MBTA-Bus-Arrival-Departure-Times_2022-05.parquet",
        engine="fastparquet",
    )
    # 1
    data = data[data["standard_type"] == "Headway"]  # 1

    data["headway_ratio"] = data["headway"] / data["scheduled_headway"]  # 2

    by_route = data.groupby(["route_id", "direction_id"])  # 3
    median_headway = by_route[["headway_ratio"]].median()

    return median_headway.nlargest(5, columns=["headway_ratio"])  # 4


tracemalloc.start()
start = time.time()
print(find_worst_headways())
end = time.time()
print(
    "Peak memory usage was {}-bytes and computation took {} seconds.".format(
        tracemalloc.get_traced_memory()[1], end - start
    )
)
tracemalloc.stop()
# Peak memory usage was 312539772-bytes and computation took 0.9239919185638428 seconds.


# Load in chunks (with fastparquet engine)
def find_worst_headways():
    chunks = []
    parquet_file = fast_pq.ParquetFile(
        "./Reduce memory usage/Pandas/MBTA-Bus-Arrival-Departure-Times_2022-05.parquet"
    )
    # 1
    for chunk in parquet_file.iter_row_groups():
        # 2
        chunk["headway_ratio"] = chunk["headway"] / chunk["scheduled_headway"]

        chunks.append(chunk[["route_id", "direction_id", "headway_ratio"]])
    del parquet_file

    data = pd.concat(chunks)
    del chunks

    # 3
    by_route = data.groupby(["route_id", "direction_id"])
    median_headway = by_route[["headway_ratio"]].median()

    # 4
    return median_headway.nlargest(5, columns=["headway_ratio"])


tracemalloc.start()
start = time.time()
print(find_worst_headways())
end = time.time()
print(
    "Peak memory usage was {}-bytes and computation took {} seconds.".format(
        tracemalloc.get_traced_memory()[1], end - start
    )
)
tracemalloc.stop()
# Peak memory usage was 354227289-bytes and computation took 0.9510002136230469 seconds.


# Polars implementation
def headways_sorted_worst_first():
    data = pl.scan_parquet(
        "./Reduce memory usage/Pandas/MBTA-Bus-Arrival-Departure-Times_2022-05.parquet"
    )

    # 1
    data = data.filter(pl.col("standard_type") == "Headway").select(
        [
            pl.col("route_id"),
            pl.col("direction_id"),
            # 2
            pl.col("headway") / pl.col("scheduled_headway"),
        ]
    )

    # 3
    by_route = data.groupby(["route_id", "direction_id"])
    median_headway = by_route.agg(pl.col("headway").median())

    # 4
    return median_headway.sort("headway", reverse=True)


# Create the query:
query = headways_sorted_worst_first()

tracemalloc.start()
start = time.time()
# Actually run the query:
result = query.collect()
end = time.time()
print(
    "Peak memory usage was {}-bytes and computation took {} seconds.".format(
        tracemalloc.get_traced_memory()[1], end - start
    )
)
tracemalloc.stop()
# Print the 5 worst headways:
print(result[:5, :])
# Peak memory usage was 15777-bytes and computation took 0.08408522605895996 seconds.
