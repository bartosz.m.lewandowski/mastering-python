import pandas as pd
import tracemalloc
from functools import reduce


# Classic read_csv()
tracemalloc.start()
df = pd.read_csv("./Reduce memory usage/Pandas/Voters.csv")["party_detailed"]
print("Size of data is {}-bytes.".format(df.memory_usage(deep=True)))
print(df.value_counts())
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()

# Reading csv in chunks
tracemalloc.start()
result = None
for chunk in pd.read_csv("./Reduce memory usage/Pandas/Voters.csv", chunksize=50):
    party = chunk["party_detailed"]
    chunk_result = party.value_counts()
    if result is None:
        result = chunk_result
    else:
        result = result.add(chunk_result, fill_value=0)

result.sort_values(ascending=False, inplace=True)
print("Size of data is {}-bytes.".format(result.memory_usage(deep=True)))
print(result)
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()
# ~40% less RAM usage then in default


# The MapReduce
def get_counts(chunk):
    party = chunk["party_detailed"]
    return party.value_counts()


def add(previous_results, new_results):
    return previous_results.add(new_results, fill_value=0)


# MapReduce structure
tracemalloc.start()
chunks = pd.read_csv("./Reduce memory usage/Pandas/Voters.csv", chunksize=50)
processed_chunks = map(get_counts, chunks)
result = reduce(add, processed_chunks)

result.sort_values(ascending=False, inplace=True)
print("Size of data is {}-bytes.".format(result.memory_usage(deep=True)))
print(result)
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()
# ~55% less RAM usage then in default


# From full to chunked
def get_counts(chunk):
    party = chunk["party_detailed"]
    return party.value_counts()


tracemalloc.start()
result = get_counts(pd.read_csv("./Reduce memory usage/Pandas/Voters.csv"))
print("Size of data is {}-bytes.".format(result.memory_usage(deep=True)))
print(result)
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()
