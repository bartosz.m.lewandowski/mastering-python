import pandas as pd


# Technique #1 - loading only the necessary columns
df = pd.read_csv("./Reduce memory usage/Pandas/voters.csv")
df.info(verbose=False, memory_usage="deep")
df.columns

df = df[["state", "candidate"]]
df.info(verbose=False, memory_usage="deep")

df = pd.read_csv(
    "./Reduce memory usage/Pandas/voters.csv", usecols=["state", "candidate"]
)
df.info(verbose=False, memory_usage="deep")

# Technique #2 - shrink numerical data (smaller dtypes)
# int8 can store integers from -128 to 127.
# int16 can store integers from -32768 to 32767.
# int64 can store integers from -9223372036854775808 to 9223372036854775807
# Important - int64 use 4x much memory as int16 (64/16=4) and 8X as much as int8 (64/8=8)

df = pd.read_csv("./Reduce memory usage/Pandas/voters.csv")
print(
    "Column type is {} while its size is {}-bytes.".format(
        df["year"].dtype, df["year"].memory_usage(index=False, deep=True)
    )
)
df["year"].max()
df["year"].min()

df = pd.read_csv("./Reduce memory usage/Pandas/voters.csv", dtype={"year": "int16"})
print(
    "Column type is {} while its size is {}-bytes.".format(
        df["year"].dtype, df["year"].memory_usage(index=False, deep=True)
    )
)

# Technique #3 - shrink categorical data (categorical dtypes)
df = pd.read_csv("./Reduce memory usage/Pandas/voters.csv")
print(
    "Column type is {} with {} variables, while its size is {}-bytes.".format(
        df["state_po"].dtype,
        set(df["state_po"]),
        df["state_po"].memory_usage(index=False, deep=True),
    )
)

df = pd.read_csv(
    "./Reduce memory usage/Pandas/voters.csv", dtype={"state_po": "category"}
)
print(
    "Column type is {} with {} variables, while its size is {}-bytes.".format(
        df["state_po"].dtype,
        set(df["state_po"]),
        df["state_po"].memory_usage(index=False, deep=True),
    )
)
df["state_po"].head()

# Technique #4 - sparse ("rozrzedzone") series for vectors with nulls
df = pd.read_csv("./Reduce memory usage/Pandas/voters.csv")
print(
    "Column type is {} with {} null values and {} length, while its size is {}-bytes.".format(
        df["candidate"].dtype,
        df["candidate"].isna().sum(),
        len(df["candidate"]),
        df["candidate"].memory_usage(index=False, deep=True),
    )
)

df["candidate"] = df["candidate"].astype("Sparse[str]")
print(
    "Column type is {} with {} null values and {} length, while its size is {}-bytes.".format(
        df["candidate"].dtype,
        df["candidate"].isna().sum(),
        len(df["candidate"]),
        df["candidate"].memory_usage(index=False, deep=True),
    )
)
# TODO: Why didn't it work?
