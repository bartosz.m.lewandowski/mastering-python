/*
DROP TABLE voters
TRUNCATE TABLE voters
RESTART IDENTITY;
*/
CREATE TABLE voters (
 	year smallint,
	state VARCHAR(25),
	state_po VARCHAR(10),
	state_fips smallint,
	state_cen smallint,
	state_ic smallint,
	office VARCHAR(100),
	candidate VARCHAR(100),
	party_detailed VARCHAR(100),
	writein VARCHAR(10),
	candidatevotes integer,
	totalvotes integer,
	version VARCHAR(100),
	notes VARCHAR(100),
	party_simplified VARCHAR(100)
)
------------------------------------------
COPY Voters
FROM 'C:\Users\User\Desktop\Data Science\mastering-python\Reduce memory usage\Pandas\Voters.csv'
DELIMITER ','
CSV HEADER;
------------------------------------------
SELECT * FROM public.voters