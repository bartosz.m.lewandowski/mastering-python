import pandas as pd
import tracemalloc
import time
from functools import reduce
import dask.dataframe as dd


"""
# making "bigdata" set
df = pd.read_csv("./Reduce memory usage/Pandas/Voters.csv")
df_bigdata = df.append([df] * (1_000 - 1), ignore_index=True)  # ~4mln rows (~500mb)
df_bigdata.to_csv("./Reduce memory usage/Pandas/Voting_bigdata.csv")
"""

# MapReduce
tracemalloc.start()
start = time.time()

# 1. Load. Read the data in chunks of 40000 records at a time.
chunks = pd.read_csv(
    "Reduce memory usage/Pandas/Voting_bigdata.csv",
    chunksize=40000,
    usecols=["candidate", "party_detailed"],
)


# 2. Map. For each chunk, calculate the per-street counts:
def get_counts(chunk):
    by_party = chunk.groupby("party_detailed")
    candidate = by_party["candidate"]
    return candidate.value_counts()


processed_chunks = map(get_counts, chunks)


# 3. Reduce. Combine the per-chunk voter counts:
def add(previous_result, new_result):
    return previous_result.add(new_result, fill_value=0)


result = reduce(add, processed_chunks)

# 4. Post-process.
result.sort_values(ascending=False, inplace=True)
end = time.time()

print(result)
print(
    "Peak memory usage was {}-bytes and computation took {} seconds.".format(
        tracemalloc.get_traced_memory()[1], end - start
    )
)
tracemalloc.stop()
# Peak memory usage was 4155160-bytes and computation took 7.010379791259766 seconds.

# All data at once
tracemalloc.start()
start = time.time()

df = pd.read_csv(
    "Reduce memory usage/Pandas/Voting_bigdata.csv",
    usecols=["candidate", "party_detailed"],
)


def get_counts(df):
    by_party = df.groupby("party_detailed")
    candidate = by_party["candidate"]
    return candidate.value_counts()


result = get_counts(df)
result.sort_values(ascending=False, inplace=True)
end = time.time()

print(result)
print(
    "Peak memory usage was {}-bytes and computation took {} seconds.".format(
        tracemalloc.get_traced_memory()[1], end - start
    )
)
tracemalloc.stop()
# Peak memory usage was 294900291-bytes and computation took 6.204000949859619 seconds.


# Using parallel computing with Dask
tracemalloc.start()
start = time.time()

# Load the data with Dask instead of Pandas.
df = dd.read_csv(
    "Reduce memory usage/Pandas/Voting_bigdata.csv",
    blocksize=16 * 1024 * 1024,  # 16MB chunks
    usecols=["candidate", "party_detailed"],
)


# Setup the calculation graph; unlike Pandas code, no work is done at this point:
def get_counts(df):
    by_party = df.groupby("party_detailed")
    candidate = by_party["candidate"]
    return candidate.value_counts()


result = get_counts(df)

# Actually run the computation, using x threads:
threads = 1
result = result.compute(num_workers=threads)

# Sort using normal Pandas DataFrame, since Dask's Pandas emulation doesn't implement this method:
result.sort_values(ascending=False, inplace=True)
end = time.time()

print(result)
print(
    "Peak memory usage was {}-bytes and computation took {} seconds (with {} threads).".format(
        tracemalloc.get_traced_memory()[1], end - start, threads
    )
)
tracemalloc.stop()
# Peak memory usage was 40360507-bytes and computation took 7.221220970153809 seconds (with 1 threads).
# Peak memory usage was 77811769-bytes and computation took 5.386942148208618 seconds (with 2 threads).
# Peak memory usage was 152472793-bytes and computation took 4.475131034851074 seconds (with 4 threads).
# Peak memory usage was 303047509-bytes and computation took 4.35300350189209 seconds (with 8 threads).
