import pandas as pd
from random import random


# Technique #1 - change numeric representations
df = pd.read_csv("./Reduce memory usage/Pandas/Voters.csv")
print(
    "Column type is {} while its size is {}-bytes.".format(
        df["candidatevotes"].dtype,
        df["candidatevotes"].memory_usage(index=False, deep=True),
    )
)

df["candidatevotes"] = list(
    df.groupby(by=["totalvotes"]).apply(
        lambda x: round(x["candidatevotes"] / x["totalvotes"] * 100)
    )
)
df["candidatevotes"] = df["candidatevotes"].astype("int8")

print(
    "Column type is {} while its size is {}-bytes.".format(
        df["candidatevotes"].dtype,
        df["candidatevotes"].memory_usage(index=False, deep=True),
    )
)

df.to_csv("./Reduce memory usage/Pandas/Voting_smaller.csv")
df = pd.read_csv(
    "./Reduce memory usage/Pandas/Voting_smaller.csv", dtype={"candidatevotes": "int8"}
)
print(
    "Column type is {} while its size is {}-bytes.".format(
        df["candidatevotes"].dtype,
        df["candidatevotes"].memory_usage(index=False, deep=True),
    )
)


# Technique #2 - sampling
def sample(row_number):
    if row_number == 0:
        # never drop the row with column names
        return False
    return random() > 0.01


df = pd.read_csv("./Reduce memory usage/Pandas/Voters.csv")
sampled = pd.read_csv("./Reduce memory usage/Pandas/Voters.csv", skiprows=sample)
print(
    "Number of samples is {} ({} before) while its size is {}-bytes ({}-bytes before).".format(
        len(sampled),
        len(df),
        sampled.memory_usage(index=False, deep=True).sum(),
        df.memory_usage(index=False, deep=True).sum(),
    )
)
