from random import random
import sys
import pandas as pd


# A Python list of strings generated from random numbers:
prefix = sys.argv[0]
random_strings = [prefix + str(random()) for i in range(1_000_000)]
print(random_strings[0:10])

# The default dtype, object:
object_dtype = pd.Series(random_strings)
print(f"Object size is {object_dtype.memory_usage(deep=True)}-bytes (default dtype).")

# A normal Pandas string dtype:
standard_dtype = pd.Series(random_strings, dtype="string")
print(f"String size is {standard_dtype.memory_usage(deep=True)}-bytes (string dtype).")

# The new Arrow string dtype from Pandas 1.3:
arrow_dtype = pd.Series(random_strings, dtype="string[pyarrow]")
print(f"Arrow size is {arrow_dtype.memory_usage(deep=True)}-bytes (arrow dtype).")

# Notice that objects/strings takes 67 bytes themselves and 8 bytes for NumPy pointers, i.e.
# (67 + 8) * 1_000_000 = 75_000_000
# when pyarrow only needs ~22 bytes, i.e.
# 22_270_671 / 1_000_000 = 22.270671
