# WORKS ONLY IN LINUX
import sys
from resource import getrusage, RUSAGE_SELF  # <- works only in linux
import numpy as np

from skimage import data
from skimage.feature import register_translation
from scipy.ndimage import fourier_shift
from skimage.transform import rescale

# Load and resize a sample image included in scikit-image:
image = data.camera()
image = rescale(image, int(sys.argv[1]), anti_aliasing=True)

# Register the image against itself; the answer should
# always be (0, 0), but that's fine, right now we just care
# about memory usage.
shift, error, diffphase = register_translation(image, image)

print("Image size (Kilo pixels):", image.size / 1024)
print("Peak memory (MiB):", int(getrusage(RUSAGE_SELF).ru_maxrss / 1024))

np.polyfit([256, 1024, 2304, 4096], [116, 176, 277, 417], 1)


def expected_memory_usage(image_pixels):
    return 7.84e-02 * (image_pixels / 1024) + 9.59e01


expected_memory_usage(256 * 1024)
expected_memory_usage(4096 * 1024)
expected_memory_usage(9999999 * 1024)
