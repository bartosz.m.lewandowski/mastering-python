import json
import ijson
import sys
from pympler import asizeof
import tracemalloc
import pandas as pd

tracemalloc.start()
with open(
    "./Reduce memory usage/Data management techniques/large-file.json",
    "r",
    errors="ignore",
) as f:
    data = json.load(f)
# load_json(data)
print("Size of raw data is {}-bytes".format(asizeof.asizeof(data)))
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()


tracemalloc.start()
user_to_repos = {}
for record in data:
    user = record["actor"]["login"]
    repo = record["repo"]["name"]
    if user not in user_to_repos:
        user_to_repos[user] = set()
    user_to_repos[user].add(repo)
print("Size of prepared dictionary is {}-bytes".format(asizeof.asizeof(user_to_repos)))
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()

# But still, peak RAM usage is ~125MiB due to loading whole file into memory before parsing (load() function)
# def load(fp, *, cls=None, object_hook=None, parse_float=None,
#         parse_int=None, parse_constant=None, object_pairs_hook=None, **kw):
#     """Deserialize ``fp`` (a ``.read()``-supporting file-like object containing
#     a JSON document) to a Python object.
#     ...
#     """
#     return loads(fp.read(), ...)

# DIGRESSION - string memory representation
s = "B" * 1000
print(
    "{}-characters string of {}'s is {}-bytes.".format(
        len(s), list(s)[0], sys.getsizeof(s)
    )
)

s2 = "ć" * 1000
print(
    "{}-characters string of {}'s is {}-bytes.".format(
        len(s2), list(s2)[0], sys.getsizeof(s2)
    )
)

# Doesn't work for some reason
# s3 = "💵" * 1000
# print("{}-characters string of {}'s is {}-bytes.".format(len(s3), list(s3)[0], sys.getsizeof(s3)))

tracemalloc.start()
user_to_repos = {}
with open(
    "./Reduce memory usage/Data management techniques/large-file.json", "rb"
) as f:
    for record in ijson.items(
        f, "item"
    ):  # each item in the top-level list we’re iterating over
        user = record["actor"]["login"]
        repo = record["repo"]["name"]
        if user not in user_to_repos:
            user_to_repos[user] = set()
        user_to_repos[user].add(repo)
print(
    "Size of prepared dictionary while reading only needed objects (using ijson) is {}-bytes".format(
        asizeof.asizeof(user_to_repos)
    )
)
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()

tracemalloc.start()
pd.read_json("./Reduce memory usage/Data management techniques/large-file.json")
print("Peak memory usage was {}-bytes.".format(tracemalloc.get_traced_memory()[1]))
tracemalloc.stop()
