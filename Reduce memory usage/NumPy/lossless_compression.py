import sparse, numpy as np

# Technique #1 - smaller dtypes
int64arr = np.ones((1024, 1024), dtype=np.uint64)
int16arr = np.ones((1024, 1024), dtype=np.uint16)
print(
    "Number of bytes for 1024x1024 'matrix' with:\
      \n- np.uint64 as dtype is {}-bytes\
      \n- np.uint16 as dtype is {}-bytes ".format(
        int64arr.nbytes, int16arr.nbytes
    )
)

# Technique #2 - sparse arrays
arr = np.random.random((1024, 1024))
arr[arr < 0.9] = 0
sparse_arr = sparse.COO(arr)
print(
    "Number of bytes for 1024x1024 'matrix':\
      \n- regular array is {}-bytes\
      \n- sparse array is  {}-bytes ".format(
        arr.nbytes, sparse_arr.nbytes
    )
)
